function hourToDecimal(hour) {
  var pieces = hour.split(':').map(p => parseInt(p));

  return pieces[0] + pieces[1]/60;
}

class ScheduleItem extends HTMLElement {
  static observedAttributes = [
    'title', 'author', 'place', 'from', 'to', 'day', 'area', 'curtime',
  ];

  constructor() {
    super();

    const templateContent = document.getElementById("schedule-item-template").content;
    const shadowRoot = this.attachShadow({ mode: "open" });

    shadowRoot.appendChild(templateContent.cloneNode(true));
  }

  attributeChangedCallback(name, old, newVal) {
    if (name === 'title') {
      this.shadowRoot.getElementById('title').innerText = newVal;
    } else if (name === 'author') {
      this.shadowRoot.getElementById('author').innerText = newVal;
    } else if (name === 'place') {
      this.shadowRoot.getElementById('place').innerText = newVal;
    } else if (name === 'from') {
      this.shadowRoot.getElementById('from').innerText = newVal;
    } else if (name === 'to') {
      this.shadowRoot.getElementById('to').innerText = newVal;
    } else if (name === 'day') {
      this.shadowRoot.getElementById('day').innerText = (new Date(newVal + 'T00:00:00')).toLocaleDateString(undefined, {day: 'numeric', month: 'short'});
    } else if (name === 'area') {
      this.shadowRoot.getElementById('area').innerText = newVal;
    } else if (name === 'curtime') {
      this.shadowRoot.getElementById('progress').style.width = this.progressBarWidth();
    }
  }

  progressBarWidth() {
    const curdate = strdate();

    if (curdate < this.getAttribute('day')) {
      return '0%';
    } else if (curdate > this.getAttribute('day')) {
      return '100%';
    }

    const from_hour = hourToDecimal(this.getAttribute('from'));
    const to_hour = hourToDecimal(this.getAttribute('to'));
    const cur_hour = hourToDecimal(strtime());
    const value = (cur_hour - from_hour)/(to_hour - from_hour)*100;

    if (value < 0) {
      return '0%';
    } else if (value > 100) {
      return '100%';
    } else {
      return `${value}%`;
    }
  }
}

customElements.define('schedule-item', ScheduleItem);
