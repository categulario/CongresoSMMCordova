function filteredTalks(talks, query) {
  return talks.filter((t) => t.blob.includes(query));
}

class TalkList extends HTMLElement {
  static observedAttributes = ['talks', 'curtime'];

  constructor() {
    super();

    const templateContent = document.getElementById("talk-list-template").content;
    const shadowRoot = this.attachShadow({ mode: "open" });

    shadowRoot.appendChild(templateContent.cloneNode(true));

    this.talks = [];
  }

  connectedCallback() {
    this.talks = JSON.parse(this.getAttribute('talks'));
    const searchInput = this.shadowRoot.getElementById('search-input');

    searchInput.addEventListener('input', (event) => {
      this.renderList(filteredTalks(
        this.talks,
        event.target.value,
      ));
    });

    this.shadowRoot.getElementById('search-clear').addEventListener('click', () => {
      searchInput.value = '';
      this.renderList(this.talks);
    });
  }

  attributeChangedCallback(name, old, newVal) {
    if (name === 'talks') {
      this.talks = JSON.parse(newVal);
      this.renderList(this.talks);
    } else if (name === 'curtime') {
      this.shadowRoot.querySelectorAll('schedule-item').forEach((el) => {
        el.setAttribute('curtime', newVal);
      });
    }
  }

  renderList(talks) {
    const talksEl = this.shadowRoot.getElementById('talks');

    talksEl.innerHTML = '';

    for (let talk of talks) {
      const talkEl = document.createElement('schedule-item');

      talkEl.setAttribute('title', talk.title);
      talkEl.setAttribute('author', talk.author);
      talkEl.setAttribute('place', talk.place);
      talkEl.setAttribute('from', talk.from);
      talkEl.setAttribute('to', talk.to);
      talkEl.setAttribute('day', talk.day);
      talkEl.setAttribute('area', talk.area);
      talkEl.setAttribute('curtime', this.getAttribute('curtime'));

      talksEl.appendChild(talkEl);
    }
  }
}

customElements.define('talk-list', TalkList);
