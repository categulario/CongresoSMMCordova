# Schedule webapp

Just as the name suggests. This is a fully-static, self-hosted, easy to mantain,
future proof way of navigating the schedule for an event.

## History

Originally developed for the Mexican Society of Mathematics (SMM) as a
[Cordova](https://cordova.apache.org/) mobile app with
[Vue](https://vuejs.org/), it is being translated to [Web
Components](https://developer.mozilla.org/en-US/docs/Web/API/Web_components) to
make it future proof.

## Development

Just run a local server with document root at the `src` folder. Modify
`src/talks.json` using current dates for the talks.

## Deployment

Put the files in the `src` folder somewhere under the document root of your http
server of choice. Deploy a `talks.json` file with your talk data.
